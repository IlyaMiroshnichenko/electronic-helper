--DeviceDto Type
insert into devicetype (id, type) values ('6aaab570-84c8-4603-8b73-38cfd75c9918', 'Overdrive');
insert into devicetype (id, type) values ('5d0a139a-5667-11ea-8e2d-0242ac130003', 'Fuzz');

--DeviceDto
insert into device (id, name, devicetypeid) values ('e7760672-55ba-11ea-a2e3-2e728ce88125', 'DOD 250 Overdrive', '6aaab570-84c8-4603-8b73-38cfd75c9918');
insert into device (id, name, devicetypeid) values ('6f9f7842-5667-11ea-82b4-0242ac130003', 'Fuzz Face', '5d0a139a-5667-11ea-8e2d-0242ac130003');

--Resistor
insert into resistor (id, nominal, power, accuracy, ispotentiometer, potentiometertype) values ('8346e2f6-55bb-11ea-a2e3-2e728ce88125', 4700, 0.25, 1, false, null);

--Capacitor Type
insert into capacitortype (id, type) values ('1b17c41a-55bc-11ea-a2e3-2e728ce88125', 'Electrolytic');

--Capacitor
insert into capacitor (id, nominal, maxpower, capacitorType) values ('437a95fe-55bc-11ea-a2e3-2e728ce88125', 0.0001, 16, '1b17c41a-55bc-11ea-a2e3-2e728ce88125');

--Diode
insert into diode (id, name) values ('8e3f4936-55bc-11ea-a2e3-2e728ce88125', '1N4001');

--OpAmp
insert into opamp (id, name, style, channel) values ('c0ad02b4-55bc-11ea-a2e3-2e728ce88125', 'UA741', 'DIP-8', '1');

--Component Type
insert into componenttype (id, type) values ('7fbc0694-55bf-11ea-a2e3-2e728ce88125', 'Resistor');
insert into componenttype (id, type) values ('7fbc0928-55bf-11ea-a2e3-2e728ce88125', 'Capacitor');
insert into componenttype (id, type) values ('7fbc0a7c-55bf-11ea-a2e3-2e728ce88125', 'Diode');
insert into componenttype (id, type) values ('7fbc0ba8-55bf-11ea-a2e3-2e728ce88125', 'OpAmp');

--Component
insert into component (id, componentid, componenttypeid) values ('d69a10e6-55bf-11ea-a2e3-2e728ce88125', '8346e2f6-55bb-11ea-a2e3-2e728ce88125', '7fbc0694-55bf-11ea-a2e3-2e728ce88125');
insert into component (id, componentid, componenttypeid) values ('d69a14a6-55bf-11ea-a2e3-2e728ce88125', '437a95fe-55bc-11ea-a2e3-2e728ce88125', '7fbc0928-55bf-11ea-a2e3-2e728ce88125');
insert into component (id, componentid, componenttypeid) values ('d69a173a-55bf-11ea-a2e3-2e728ce88125', '8e3f4936-55bc-11ea-a2e3-2e728ce88125', '7fbc0a7c-55bf-11ea-a2e3-2e728ce88125');
insert into component (id, componentid, componenttypeid) values ('d69a188e-55bf-11ea-a2e3-2e728ce88125', 'c0ad02b4-55bc-11ea-a2e3-2e728ce88125', '7fbc0ba8-55bf-11ea-a2e3-2e728ce88125');

--DeviceDto Configuration
insert into deviceconfiguration (deviceid, componentid) values ('e7760672-55ba-11ea-a2e3-2e728ce88125', 'd69a10e6-55bf-11ea-a2e3-2e728ce88125');
insert into deviceconfiguration (deviceid, componentid) values ('e7760672-55ba-11ea-a2e3-2e728ce88125', 'd69a14a6-55bf-11ea-a2e3-2e728ce88125');
insert into deviceconfiguration (deviceid, componentid) values ('e7760672-55ba-11ea-a2e3-2e728ce88125', 'd69a173a-55bf-11ea-a2e3-2e728ce88125');
insert into deviceconfiguration (deviceid, componentid) values ('e7760672-55ba-11ea-a2e3-2e728ce88125', 'd69a188e-55bf-11ea-a2e3-2e728ce88125');