package ru.semiconductors.electronichelper.dao.service;

import org.springframework.stereotype.Service;
import ru.semiconductors.electronichelper.dao.entity.DeviceEntity;

import java.util.List;

@Service
public interface DeviceDaoService {
    List<DeviceEntity> findAll();
}
