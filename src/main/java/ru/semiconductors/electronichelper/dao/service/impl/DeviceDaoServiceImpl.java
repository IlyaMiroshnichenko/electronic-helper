package ru.semiconductors.electronichelper.dao.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.semiconductors.electronichelper.dao.entity.DeviceEntity;
import ru.semiconductors.electronichelper.dao.service.DeviceDaoService;

import java.util.List;

@Service
public class DeviceDaoServiceImpl implements DeviceDaoService {

    @Autowired
    private DeviceDaoService deviceDaoService;

    @Override
    public List<DeviceEntity> findAll() {
        return deviceDaoService.findAll();
    }
}
