package ru.semiconductors.electronichelper.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.semiconductors.electronichelper.dao.entity.DeviceTypeEntity;

@Repository
public interface DeviceTypeRepository extends JpaRepository<DeviceTypeEntity, Long> {
}
