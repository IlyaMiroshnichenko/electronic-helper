package ru.semiconductors.electronichelper.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.semiconductors.electronichelper.dao.entity.DeviceEntity;

@Repository
public interface DeviceRepository extends JpaRepository<Long, DeviceEntity> {
}
