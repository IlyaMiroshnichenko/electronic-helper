package ru.semiconductors.electronichelper.dao.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "device")
public class DeviceEntity implements Serializable {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "deviceType", referencedColumnName = "id")
    private DeviceTypeEntity deviceType;

    public DeviceEntity() {
    }

    public DeviceEntity(Long id, String name, DeviceTypeEntity deviceType) {
        this.id = id;
        this.name = name;
        this.deviceType = deviceType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DeviceTypeEntity getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceTypeEntity deviceType) {
        this.deviceType = deviceType;
    }
}
