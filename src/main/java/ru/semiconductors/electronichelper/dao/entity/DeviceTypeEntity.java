package ru.semiconductors.electronichelper.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;


@Entity
@Table(name = "deviceType")

public class DeviceTypeEntity implements Serializable {

    @Id
    @Column(name = "id")
    @JsonIgnore
    private Long id;

    @Column(name = "type")
    private String type;

    @OneToOne(mappedBy = "deviceTypeId")
    @JsonIgnore
    private DeviceEntity deviceEntity;

    public DeviceTypeEntity() {
    }

    public DeviceTypeEntity(Long id, String type) {
        this.id = id;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DeviceEntity getDeviceEntity() {
        return deviceEntity;
    }

    public void setDeviceEntity(DeviceEntity deviceEntity) {
        this.deviceEntity = deviceEntity;
    }
}
