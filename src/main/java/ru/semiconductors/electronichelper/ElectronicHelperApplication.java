package ru.semiconductors.electronichelper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElectronicHelperApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElectronicHelperApplication.class, args);
	}

}
